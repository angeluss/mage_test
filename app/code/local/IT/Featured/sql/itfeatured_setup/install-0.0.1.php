<?php
/**
 * @category  IT
 * @package   Featured
 */
$installer = $this;

$installer->addAttribute(Mage_Catalog_Model_Product::ENTITY, 'is_featured', array(
    'group'                      => 'Promotions',
    'type'                       => 'int',
    'input'                      => 'select',
    'label'                      => 'Featured',
    'source'                     => 'eav/entity_attribute_source_boolean',
    'visible'                    => 1,
    'sort_order'                 => 10,
    'required'                   => 0,
    'user_defined'               => 1,
    'searchable'                 => 0,
    'filterable'                 => 0,
    'comparable'                 => 0,
    'visible_on_front'           => 0,
    'visible_in_advanced_search' => 0,
    'is_html_allowed_on_front'   => 0,
    'is_configurable'            => 1,
    'global'                     => true,
    'backend'                    => '',
));

$installer->endSetup();

