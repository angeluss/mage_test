<?php
/**
 * @category  IT
 * @package   Featured
 */
class IT_Featured_IndexController extends Mage_Core_Controller_Front_Action
{
    /**
     * Prepare indexAction
     */
    public function indexAction()
    {
        $this->loadLayout();
        $this->renderLayout();
    }

}